package com.kenyo;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ch.qos.logback.classic.Logger;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@SpringBootApplication
public class SpringReactorDemoApplication implements CommandLineRunner {
	
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(SpringReactorDemoApplication.class);
	
	private static List<String> platos = new ArrayList<>();

	public static void main(String... args) {
		platos.add("Hamburguesa");
		platos.add("Pizza");
		
		SpringApplication.run(SpringReactorDemoApplication.class, args);
	}
	
	public void crearMono() {
		Mono<Integer> numero = Mono.just(7);
		numero.subscribe(x -> log.info("Número: " + x));
	}
	
	public void crearFlux() {
		Flux<String> observablePlatos = Flux.fromIterable(platos);
		observablePlatos.subscribe(p -> log.info(p));
		
		// de flux a mono
		
		observablePlatos.collectList().subscribe(lista -> log.info(lista.toString()));
	}
	
	public void m1doOnNext() { // para temas de depuración
		Flux<String> observablePlatos = Flux.fromIterable(platos);
		observablePlatos.doOnNext(p -> log.info(p)).subscribe(); 
	}
	
	
	public void m2Map() {
		Flux<String> observablePlatos = Flux.fromIterable(platos);
		observablePlatos.map( p -> "Plato: " + p).subscribe( p -> log.info(p));
	}
	
	public void m3FlatMap() {
		Mono.just("Joel").flatMap(x -> Mono.just(29)).subscribe(rpta -> log.info("rpta: " +rpta));
	}
	
	public void m4Range() {
		Flux<Integer> obs = Flux.range(0, 10);
		obs = obs.map(x -> {
			return x +1;
		});
		
		obs.subscribe(x -> log.info("N: " + x));
	}
	
	public void  m5DelayElements() {
		Flux<Integer> obs1 = Flux.range(0, 10)
								.delayElements(Duration.ofSeconds(2))
								.doOnNext( x -> log.info(x.toString()));
		obs1.blockLast(); // indica cuando todos los valores hayan cambiado
	}
	
	
	public void m6ZipWith() {
		List<String> clientes = new ArrayList<>();
		clientes.add("Joel");
		clientes.add("Code");
		
		Flux<String> obsPlatos = Flux.fromIterable(platos);
		Flux<String> obsClientes = Flux.fromIterable(clientes);
		
		obsPlatos.zipWith(obsClientes, (p, c) -> String.format("Flux1: %s, Flux2 %s", p, c)).subscribe(x -> log.info(x));
		
	}
	
	public void m7Merge() {
		List<String> clientes = new ArrayList<>();
		clientes.add("Joel");
		clientes.add("Code");
		
		Flux<String> obsPlatos = Flux.fromIterable(platos);
		Flux<String> obsClientes = Flux.fromIterable(clientes);
		
		Flux.merge(obsPlatos, obsClientes).subscribe(x -> log.info(x));
	}
	
	public void m8Filter() {
		Flux<String> obsPlatos = Flux.fromIterable(platos);
		
		obsPlatos.filter(p -> {
			return p.startsWith("H");
		}).subscribe(x -> log.info(x));
	}
	
	
	public void m9Takelast() {
		Flux<String> obsPlatos = Flux.fromIterable(platos);
		
		obsPlatos.takeLast(3).subscribe(x -> log.info(x));
	}
	
	public void m10Take() {
		Flux<String> obsPlatos = Flux.fromIterable(platos);
		obsPlatos.take(1).subscribe(x -> log.info(x));
		obsPlatos.take(1).subscribe(x -> log.info(x));
	}
	
	public void m11DefaultIfEmpty() {
		platos = new ArrayList<>();
		Flux<String> obsPlatos = Flux.fromIterable(platos);
		
		obsPlatos.defaultIfEmpty("ESTA VACIO").subscribe(x -> log.info(x));
		
		//Flux.empty();
	}
	
	public void m12OnErrorReturn() {
		Flux<String> obsPlatos = Flux.fromIterable(platos);
		
		obsPlatos.doOnNext(p -> {
			throw new ArithmeticException("mal clculo");
		})
		//.onErrorMap(ex -> new ArithmeticException("mal calculo"))
		.onErrorReturn("ERROR")
		.subscribe(x -> log.info(x));
	}
	
	public void m13Retry() {
		Flux<String> obsPlatos = Flux.fromIterable(platos);
		
		obsPlatos.doOnNext(p -> {
			log.info("intentandoi...........");
			throw new ArithmeticException("mal clculo");
		})
		.retry(3)
		.onErrorReturn("ERROR")
		.subscribe(x -> log.info(x));
	}
	
	
	@Override
	public void run(String... args) throws Exception {
		//crearMono();
		//crearFlux();
		//m1doOnNext();
		//m2Map();
		//m3FlatMap();
		//m4Range();
		//m5DelayElements();
		//m6ZipWith();
		//m7Merge();
		//m8Filter();
		//m9Takelast();
		//m10Take();
		//m11DefaultIfEmpty();
		//m12OnErrorReturn();
		m13Retry();
	}

}
